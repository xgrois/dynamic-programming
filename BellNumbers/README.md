﻿## Bell Numbers

Use bottom-up DP to solve Bell numbers. This code uses Stirling numbers to calculate Bell numbers.
This is due to the following recurrence:

B(n) = sum_{k = 0}^{n} S(n,k)

The code also includes the calculation using the Bell triangle.

__Complexity__
* DP version: polinomial.

__References__

[[1]](https://en.wikipedia.org/wiki/Stirling_numbers_of_the_second_kind) Stirling numbers of the second kind, Wikipedia.

[[2]](https://en.wikipedia.org/wiki/Bell_number) Bell number, Wikipedia.

## Notes

Relation of Stirling numbers with Bell numbers.

![Alt text](/BellNumbers/stirling_bell.png?raw=true "Relationship between Bell number and Stirling numbers")

