﻿## Sum set problem

__Problem statement__

Given a set of non-negative integers, and a value sum, determine if there is a subset of the given set with sum equal to given sum. 

__Difficulty in literature__

This problem is considered Medium in literature.

__Runtime Complexity__

* Recursive version: exponential
* DP version (tabular): polynomial O(n x sum)

__Space Complexity__

* DP version (tabular): O(n x sum)

__References__

[[1]](https://www.geeksforgeeks.org/subset-sum-problem-dp-25/) Subset Sum Problem, GeeksforGeeks.
