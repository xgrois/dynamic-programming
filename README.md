# Practising Dynamic Programming

Practising Dynamic Programming (DP).

## Content

[[1]](/FibonacciNumbers) Fibonacci numbers.

[[2]](/UglyNumbers) Ugly numbers.

[[3]](/CatalanNumbers) Catalan numbers.

[[4]](/BellNumbers) Bell and Stirling numbers.

[[5]](/LongestCommonSubsequence) Longest Common Subsequence(s).

[[6]](/LongestIncreasingSubsequence) Longest Increasing Subsequence.

[[7]](/EditDistance) Edit Distance.

[[8]](/PartitionSum) Partition (sum) problem.

[[9]](/Knapsack) 0-1 Knapsack problem.

[[10]](/WaysToCoverADistance) Ways to cover a distance in steps of 1, 2, 3.

[[11]](/MatrixLongestIncreasingPath) Length of longest +1 increasing path in a square matrix.

[[12]](/LongestIncreasingPathInMatrix) Length of longest increasing path in a matrix.

[[13]](/SumSet) Sum set problem.

[[14]](/JobScheduling) Job Scheduling problem.


## Language
C#

## IDE

Visual Studio Community [VS 2019](https://visualstudio.microsoft.com/es/vs/).

## License
[MIT](https://choosealicense.com/licenses/mit/)
