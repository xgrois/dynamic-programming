## Job Scheduling with Profits, Deadlines and Duration

__Problem statement__

Assign jobs to slots so the total profit is maximized. You have a set of jobs, each with a duration, a deadline and a profit if completed before deadline. You cannot split a job in non-consecutive slots. You may be restricted to slots from 1 to L (schedule length).

<img src="/JobScheduling/problem.png" alt="drawing" width="800"/>

There are a few greedy-optimal algorithms to tackle this problem when all tasks can be completed in a unit of time (or slot).
When each task can last 1 or more slots, as in this case, the problem is more challeging and these solutions do not work anymore.

Here, we solve this more generic problem using Dynamic Programming.

__Difficulty in literature__

This is classic NP-Hard optimization problem and it is an advanced one. 

Lawler and Moore (1969) introduced this problem and proposed a pseudo-polynomial exact method for its solution [1]. With years, different authors 
have proposed improvements over the original solution algorithm. Even though this problem is well known, computer scientist still work on it
to provide improvements. As such, there exists quite recent contributions (2014) [2].

Here, we provide a DP algorithm to solve the problem. The good thing is that this algorithm finds the optimal solution.

__Complexity__

The DP solution has to sequential algorithms. First, jobs must be sorted by their deadlines which is O(J log J) in most typical implemented sorting algorithms. Second, the progressive calculation of the values in the DP matrix, each with constant time (just a max or = operation), hence, O(J x min(Dmax, L)). Since these two algorithms are sequential, the total complexity is the sum O(J log J + J*Lmax), where Lmax = min(Dmax,L).

__References__

[[1]](https://www.jstor.org/stable/2628367?seq=1) E.L. Lawler, Moore, J.M. (1969) "A functional equation and its
application to resource alocation and sequencing problems", Management Science 16 (1), pp. 77-84. 

[[2]](https://ieeexplore.ieee.org/document/6996878) E. Levner and A. Elalouf, "An improved approximation algorithm for the ancient scheduling problem with deadlines," 2014 International Conference on Control, Decision and Information Technologies (CoDIT), Metz, 2014, pp. 113-116, doi: 10.1109/CoDIT.2014.6996878.

## Notes

__Direct derivation__

Below some notes to derive the recurrrence relation.
Basically, the idea is to imagine that you move the duration box of a job from left to right and you ask yourself
whether you can include the job and if that would increase the profit.
You can do this if you sort jobs by deadline, so you can pick the first job an "move it from left to right", then pick the second one,
and so on so forth. This is what I did in below notes as I filled the "imaginary DP matrix".

<img src="/JobScheduling/dp_example.png" alt="drawing" width="800"/>

__Recursive to DP derivation__

This is quite convenient in many problems that can be solved using DP. However, in this particular instance,
the complexity increases significantly.

I provide some of my notes for the sake of completeness and potential interest.

<img src="/JobScheduling/recursive_data.JPG" alt="drawing" width="800"/>
<img src="/JobScheduling/recursive_tree.png" alt="drawing" width="1000"/>
<img src="/JobScheduling/recursive_recurrence.JPG" alt="drawing" width="800"/>
