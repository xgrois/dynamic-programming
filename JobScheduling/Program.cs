﻿using System;
using System.Collections.Generic;

namespace JobScheduling
{
    class Job
    {
        public int Id { get; set; }
        public int Profit { get; set; }
        public int Duration { get; set; }
        public int Deadline { get; set; }

        public Job(int id, int p, int d, int dl)
        {
            Id = id;
            Profit = p;
            Duration = d;
            Deadline = dl;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("::: Job Scheduling with profits, deadlines and duration :::");

            List<Job> jobs = new List<Job>()
            {
                new Job(1,10,1,5),
                new Job(2,60,1,3),
                new Job(3,20,1,1),
                new Job(4,50,2,3),
                new Job(5,30,1,2),
                new Job(6,10,2,2),
                
            };

            int L = 4;
            int sol = Solution.MaxProfitOf(jobs, L);

            Console.WriteLine($"\n\rMaximum profit is {sol}.");
        }
    }

    static class Solution
    {
        internal static int MaxProfitOf(List<Job> jobs, int L)
        {
            // We do not check inputs
            // Assume L >= 0
            // Assume jobs is not empty

            // Sort jobs by deadline (increasing)
            jobs.Sort((x, y) => x.Deadline.CompareTo(y.Deadline));

            // Once jobs are sorted, continue
            int Dmax = jobs[jobs.Count - 1].Deadline;
            int Lmax = Math.Min(L, Dmax);

            int[,] DP = new int[jobs.Count + 1, Lmax + 1];

            for (int j = 1; j <= jobs.Count; j++)
            {
                for (int s = 1; s <= Lmax; s++)
                {
                    int Dj = jobs[j-1].Deadline;
                    if (s> Dj)
                    {
                        DP[j, s] = DP[j, Dj];
                    }
                    else
                    {
                        int Tj = jobs[j-1].Duration;
                        if (s < Tj)
                        {
                            DP[j, s] = DP[j - 1, s];
                        }
                        else
                        {
                            int Pj = jobs[j-1].Profit;
                            DP[j, s] = Math.Max(DP[j - 1, s - Tj] + Pj, DP[j - 1, s]);
                        }
                    }
                }
            }

            Console.WriteLine();
            PrintMatrix(DP);
            Console.WriteLine();
            PrintSchedule(jobs,DP, DP.GetLength(0) - 1, DP.GetLength(1) - 1);

            return DP[jobs.Count, Lmax];
        }

        private static void PrintSchedule(List<Job> jobs, int[,] DP, int j, int s)
        {
            if (j == 0) return;
            if (DP[j, s] == DP[j - 1, s]) PrintSchedule(jobs, DP, j - 1, s);
            else
            {
                int Dj = jobs[j-1].Deadline;
                int Tj = jobs[j-1].Duration;
                int s2 = Math.Min(s, Dj) - Tj;
                PrintSchedule(jobs, DP, j - 1, s2);
                Console.WriteLine($"Schedule job {jobs[j-1].Id} at slot {s2+1}.");
            }
        }

        internal static void PrintMatrix(int[,] matrix)
        {

            for (int row = 0; row < matrix.GetLength(0); row++)
            {
                for (int col = 0; col < matrix.GetLength(1); col++)
                {

                    //string aux = string.Format("{00.0}", matrix[row, col]);
                    Console.Write(matrix[row, col].ToString().PadLeft(4, ' '));
                }
                Console.WriteLine();
            }
        }

    }
}
